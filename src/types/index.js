import people from "./people"
import nature from "./nature"

let obj = { }

Object.assign(obj, 
    people, nature
)

export default obj